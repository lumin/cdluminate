Caligraphy and Fonts
===

* Mono-spaced Font (for programming): Operator Mono > Monaco > IBM Plex > Fira Code > *

* Serief Font: Minion Pro, Times New Roman.


* Handwriting: roundhand.

* Fountain Pens: LAMY AL-Star > ...

