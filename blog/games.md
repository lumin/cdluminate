第九艺术 / Artistic Video Games
===

区间/Range [10/10]

* 神界原罪2 | Divinity: Origin Sin 2 (10/10) -- 我心目中最好的游戏。遇到宝箱没有钥匙可以砸，遇到门没有钥匙可以砸...。

区间/Range [9/10, 10/10)

* 远星物语 | CrossCode (9.5/10)
* 蔚蓝 | Celeste (9/10)
* 星界边境 | StarBound (9/10)
* 空洞骑士 | Hollow Knight (9/10)
* 博得之门 | Baldur's Gate 3 (9/10)
* 塞尔达传说：荒野之息 | Zelda: Breath of the Wild (9/10) -- 眼见之处均可及的自由，所有墙都可以爬，这很重要。
* 原神 | Genshin Impact (9/10) -- 继承了BoW的特性，又增添了大量其没有的新内容。角色好看，部分角色形象塑造令人印象深刻。

[8/10, 9/10)

* OneShot (8.5/10)
* 奥日与精灵意志 | Ori and the Will of the Wisps (8.5/10)
* 无主之地3 | Borderlands 3 (8/10)
* Limbo (8/10)
* 神界原罪 | Divinity: Origin Sin (8/10)
* Inside (8/10)
* 泰拉瑞亚 | Terraria
* 去月球 | To The Moon
* 星露谷物语 | Stardew Valley
* 极限竞速：地平线4 | Forza Horizon 4

[7/10, 8/10)

* 戴森球计划 | Dyson Sphere Program (7/10)
* Into the Breach (7/10)
* 尼尔：机械纪元 | NieR: Automata (7/10)
* 天国：拯救 | Kindom Come: Deliverance
* 都市：天际线 | Cities: Skylines
* Counter-Strike: Global Offensive
* FTL: Faster Than Light
* 地平线：零之曙光 | Horizon: Zero Dawn
* RimWorld
* 文明VI | Sid Meier's Civillization VI

[Wishlist]

* Undertale
* Akatori (BGM)
* Rune Fencer Illyia (BGM)
* Silksong
