第九艺术 / Artistic Video Games
===

我会在本文档中列出部分我玩过的游戏，并对它们做出一些简短的评价。

In this document, I will list some games I have played in the past, and make
some brief comments accordingly.

区间/Range [10/10]

* 神界原罪2 | Divinity: Origin Sin 2 (10/10) -- 我心目中最好的游戏。遇到宝箱没有钥匙可以砸，遇到门没有钥匙可以砸...。

区间/Range [9/10, 10/10)

* 远星物语 | CrossCode (9.5/10) -- 非常好的故事。
* 蔚蓝 | Celeste (9/10) -- 手感极好，训练一种节奏感。
* 星界边境 | StarBound (9/10) -- 享受一种孤独的感觉。
* 空洞骑士 | Hollow Knight (9/10) -- 梦回小时候玩过的银河恶魔城类型游戏。
* 博得之门 | Baldur's Gate 3 (9/10) -- 继承了神界原罪2的一些要素，但是玩法已经发生本质变化。玩家可以更加专注于在游戏世界中冒险，而不是满世界抠经验值培养角色。
* 塞尔达传说：荒野之息 | Zelda: Breath of the Wild (9/10) -- 眼见之处均可及的自由，所有墙都可以爬，这很重要。游戏过程中升级的不再是角色，而是玩家自己。
* 原神 | Genshin Impact (9/10) -- 继承了BoW的特性，又增添了大量其没有的新内容。角色好看，部分角色形象塑造令人印象深刻（洗脑）。

[8/10, 9/10)

* OneShot (8.5/10)
* 奥日与精灵意志 | Ori and the Will of the Wisps (8.5/10)
* 无主之地3 | Borderlands 3 (8/10)
* Limbo (8/10)
* 神界原罪 | Divinity: Origin Sin (8/10)
* Inside (8/10)
* 泰拉瑞亚 | Terraria
* 去月球 | To The Moon
* 星露谷物语 | Stardew Valley
* 极限竞速：地平线4 | Forza Horizon 4
* 艾尔登法环 | Elden Ring -- 难度更加真实的战斗，敌人不再是大部分时间站立不动的木桩。

[7/10, 8/10)

* 戴森球计划 | Dyson Sphere Program (7/10)
* Into the Breach (7/10)
* 尼尔：机械纪元 | NieR: Automata (7/10)
* 天国：拯救 | Kindom Come: Deliverance
* 都市：天际线 | Cities: Skylines
* Counter-Strike: Global Offensive
* FTL: Faster Than Light
* 地平线：零之曙光 | Horizon: Zero Dawn
* RimWorld
* 文明VI | Sid Meier's Civillization VI

[Wishlist]

* Undertale
* Akatori (BGM)
* Rune Fencer Illyia (BGM)
* Silksong
* 地平线：西之绝境 -- 故事精彩，引人入胜。
