### Research

The following table is continuously synchronized with my [Google Scholar Profile](https://scholar.google.com/citations?user=BVIO95UAAAAJ).

<table>
  <tr>
    <td><b>#ID</b></td>
    <td><b>Title</b></td>
    <td><b>Conference</b></td>
    <td><b>PDF</b></td>
    <td><b>arXiv</b></td>
    <td><b>Github</b></td>
  </tr>
  
  <tr>
    <td>C08</td>
    <td><div>Resource-Adaptive Federated Learning with All-In-One Neural Composition</div></td>
    <td>NeurIPS 2022</td>
    <td><a href="https://openreview.net/forum?id=wfel7CjOYk">Openreview</a></td>
    <td></td>
    <td></td>
  </tr>

  <tr>
    <td>X02</td>
    <td><div>On Trace of PGD-Like Adversarial Attacks</div></td>
    <td>arXiv 2022</td>
    <td></td>
    <td><a href="https://arxiv.org/abs/2205.09586">arXiv:2205.09586</a></td>
    <td></td>
  </tr>
  
  <tr>
    <td>C07</td>
    <td><div>Enhancing Adversarial Robustness for Deep Metric Learning</div></td>
    <td>CVPR 2022</td>
    <td><a href="https://openaccess.thecvf.com/content/CVPR2022/html/Zhou_Enhancing_Adversarial_Robustness_for_Deep_Metric_Learning_CVPR_2022_paper.html">OpenAccess</a></td>
    <td><a href="https://arxiv.org/abs/2203.01439">arXiv:2203.01439</a></td>
    <td><a href="https://github.com/cdluminate/robdml">RobDML</a></td>
  </tr>
  
  <tr>
    <td>X01</td>
    <td><div>Adversarial Attack and Defense in Deep Ranking</div></td>
    <td>arXiv 2021</td>
    <td></td>
    <td><a href="https://arxiv.org/abs/2106.03614">arXiv:2106.03614</a></td>
    <td><a href="https://cdluminate.github.io/robrank/">RobRank</a></td>
  </tr>

  <tr>
    <td>J01</td>
    <td>Adaptive Ladder Loss for Learning Coherent Visual-Semantic Embedding</td>
    <td>T-MM 2021</td>
    <td><a href="https://ieeexplore.ieee.org/abstract/document/9665378">IEEE Xplore</a></td>
    <td></td>
    <td></td>
  </tr>
  
  <tr>
    <td>C06</td>
    <td><div>Practical Relative Order Attack in Deep Ranking</div></td>
    <td>ICCV 2021</td>
    <td><a href="https://openaccess.thecvf.com/content/ICCV2021/html/Zhou_Practical_Relative_Order_Attack_in_Deep_Ranking_ICCV_2021_paper.html">OpenAccess</a></td>
    <td><a href="https://arxiv.org/abs/2103.05248">arXiv:2103.05248</a></td>
    <td><a href="https://github.com/cdluminate/advorder">AdvOrder</a></td>
  </tr>
  
  <tr>
    <td>C05</td>
    <td>SGCN: Sparse Graph Convolution Network for Pedestrian Trajectory Prediction</td>
    <td>CVPR 2021</td>
    <td><a href="https://openaccess.thecvf.com/content/CVPR2021/papers/Shi_SGCN_Sparse_Graph_Convolution_Network_for_Pedestrian_Trajectory_Prediction_CVPR_2021_paper.pdf">OpenAccess</a></td>
    <td><a href="https://arxiv.org/abs/2104.01528">arXiv:2104.01528</a></td>
    <td><a href="https://github.com/shuaishiliu/SGCN">SGCN</a></td>
  </tr>
  
  <tr>
    <td>C04</td>
    <td>Adversarial Ranking Attack and Defense</td>
    <td>ECCV 2020</td>
    <td><a href="https://link.springer.com/chapter/10.1007%2F978-3-030-58568-6_46">Springer</a></td>
    <td><a href="https://arxiv.org/abs/2002.11293">arXiv:2002.11293</a></td>
    <td><a href="https://cdluminate.github.io/advrank/">AdvRank</a></td>
  </tr>

  <tr>
    <td>C03</td>
    <td>Ladder Loss for Coherent Visual-Semantic Embedding</td>
    <td>AAAI 2020</td>
    <td><a href="https://ojs.aaai.org//index.php/AAAI/article/view/7006">AAAI</a></td>
    <td><a href="https://arxiv.org/abs/1911.07528">arXiv:1911.07528</a></td>
    <td><a href="https://github.com/cdluminate/ladderloss">Ladderloss</a></td>
  </tr>
  
  <tr>
    <td>C02</td>
    <td>Hierarchical Multimodal LSTM for Dense Visual-Semantic Embedding</td>
    <td>ICCV 2017</td>
    <td><a href="https://openaccess.thecvf.com/content_iccv_2017/html/Niu_Hierarchical_Multimodal_LSTM_ICCV_2017_paper.html">OpenAccess</a></td>
    <td></td>
    <td></td>
  </tr>
  
  <tr>
    <td>C01</td>
    <td>Ordinal Regression with Multiple Output CNN for Age Estimation</td>
    <td>CVPR 2016</td>
    <td><a href="https://www.cv-foundation.org/openaccess/content_cvpr_2016/html/Niu_Ordinal_Regression_With_CVPR_2016_paper.html">OpenAccess</a></td>
    <td></td>
    <td></td>
  </tr>

</table>
