Anime (Japanese) List / 动漫列表
===

## My Fav Animes | 个人新番排名

This is not an exhaustive list and it does not include all my watched animes.
My ranking standard here is very tough. A score of 3/5 is already very high
actually.  Any anime with a score higher than 3/5 must have deeply impressed me
in some way.

这是一个不完全的动漫评分列表，其并不包括所有我看过的动漫作品。
该列表的评分标准非常残酷，其实3/5分已经非常高了。任何被我给予大于3/5分评价
的动漫作品都已经至少在某一方面令我印象深刻。

Covering animes till (时间线覆盖至): *2022-Q2*

[★★★★★] 5/5

* Nichijou | 日常 (2011) -- 毫无疑问日常番的天花板，作画质量优秀，角色性格鲜明。京阿尼用不同寻常的方式表现了柔和而又平静的日常。我们所经历的每一个日常，或许就是一个又一个的奇迹。
* Kimetsu no Yaiba | 鬼灭之刃 -- 这里也包括了无限列车和花街篇。近几年少有的质量过硬的热血动漫，尤其是在我的英雄学院作者不当人的事实被揭露之后。
* Kaguya-sama wa Kokurasetai | 辉夜大小姐想让我告白 -- 是初恋的感觉，甜而不腻。

[★★★★☆] 4.5/5

* Violet Evergarden | 紫罗兰的永恒花园 -- 感受到爱的存在。Felt the existence of love.
* Hinamatsuri | 黑社会的超能力女儿
* Miss Kobayashi's Dragon Maid | 小林家的龙女仆
* Saekano: How to Raise a Boring Girlfriend | 路人女主的养成方法
* Mushoku Tensei (Season I and II) | 无职转生 (I，II季) -- 各方面肉眼可见的制作精良，剧情引人入胜。期待第III季。
* SPY×FAMILY | 间谍过家家 -- 非常精彩的设定，演出效果极佳。评分完结后再固定。

[★★★★] 4/5

* Sono Bisque Doll wa Koi wo Suru | 更衣人偶坠入爱河
* Fate/Zero (2011)
* Astro Boy | 铁壁阿童木 (2003)
* Vinland Saga | 冰海战记
* FULLMETAL ALCHEMIST | 钢之炼金术士 (2009)

[★★★★☆] 3.5/5

* 弱キャラ友崎くん | 弱势角色友崎君
* ダンベル何キロ持てる？ | 流汗吧！健身少女
* 「青春ブタ野郎」シリーズ | 青春猪头少年不会梦到兔女郎学姐
* Sound!Euphonium | 吹响吧！上低音号
* Masamune-kun no Revenge | 政宗君的复仇

[★★★] 3/5

* Fate/Stay Night | 命运之夜 (2014)
* Re:Zero | Re:从零开始的异世界生活 (2016)
* A Certain Scientific Railgun | 某科学的超电磁炮 (2009)
* A Certain Scientific Railgun | 某科学的超电磁炮S
* A Certain Scientific Railgun | 某科学的超电磁炮T
* Vivy-fluorite eye's song | 薇薇 -萤石眼之歌
* Laid-Back Camp | 摇曳露营
* Alice & Zouroku | 爱丽丝与藏六
* NEW GAME! | 新游戏
* ふらいんぐうぃっち | 飞翔的魔女
* Amagi Brilliant Park | 甘城光辉游乐园 (2014)
* 学战都市
* Overlord
* 男子高中生的日常 (2012)
* Hanasaku iroha | 花开伊吕波 (2011)
* To Your Eternity | 致不灭的你
* The Demon Girl Next Door | 街角魔族
* Gamers! | GAMERS! 电玩咖
* 私に天使が舞い降りた！ | 天使降临到我身边

[★★☆] 2.5/5

* YU-GI-OH! Duel Monster | 游戏王: 决斗怪兽
* Attack on Titan | 进击的巨人 (2013)
* My Girlfriend is Shobitch | 我的女友是个过度认真的
* 俺の彼女と幼なじみが修羅場すぎる | 我女友与青梅竹马的惨烈修罗场 (2013)
* Sword Art Online (SAO) | 刀剑神域 
* 86 | 86-不存在的战区 (2021)
* A Certain Magical Index | 魔法禁书目录 (2008)
* 王様ランキング | 国王排名 (2021) -- 开场不错，但是从前三分之一就开始剧情质量逐渐下滑到影响观感的程度，最终大幅拉低了给分，并让人后悔追番。

[☆] 0.5/5

## Anime Movies

* 宫崎骏系列
* The Shape of Voice | 声之形
* 紫罗兰的永恒花园

## American Anime

* RWBY
* 猫和老鼠

## Chinese Anime

* 仙王的日常生活
* 罗小黑战记 -- 画风非常萌；声优山新的演出水平实在令人印象深刻。
* 天书奇谈 -- 这让我联想到了"开源"
* 风灵玉秀
* 凡人修仙传

## Random Comments | 野卡评论/泛谈

## Male Protagonists

## Female Protagonists

## See Audiences through Animes | 透过作品看观众


## References

1. [萌娘百科: 日本动画总导航 | Moegirl Wiki](https://zh.moegirl.org.cn/Template:%E6%97%A5%E6%9C%AC%E5%8A%A8%E7%94%BB%E6%80%BB%E5%AF%BC%E8%88%AA)
2. [Bilibili 番剧索引 | Bilibili Anime Index]
3. Crunchyroll Anime Index
4. GoGoAnime Anime List
5. [MyAnimeList](https://myanimelist.net/topanime.php)
6. [LiveCharts](https://www.livechart.me/)
