电子产品评论 | Reviews on Electronics
===

### 笔记本电脑 | Laptops

### 显示器 | Monitors

一旦用了4K显示器，回到1080p会感觉像瞎了一样。至少也要2k才能相对舒适。

### 键盘/机械键盘 | (Mechanical) Keyboards

目前最喜欢的键盘有三个：阿米洛 Varmilo VD87 花旦娘，Logi MX Keys, Logi MX Keys Mini.

阿米洛的静电容绿轴也不错，不过不在手边很久了。

### 鼠标 | Mouse

罗技 G304 和罗技 G305 是我一直在用，价格合适的鼠标。虽然是无线，但是响应速度极快以至于无法感受到和有线鼠标的区别，且精度已经够工作使用。

联想的 Thinklife 静音小鼠标虽然静音，但是无线延迟感受非常明显，而且精度存在不足，对于用inkscape之类工具画图的任务会明显感觉到蹩脚。

### 手机 | Cellphone

### 耳机 | Headset

### 手机和笔记本电脑配件 | Mobile Phone / Laptop Accessories

* 支架

* GAN充电器

* TypeC线缆

### 台式机 | Desktop Computer

### 打印机 | Printers

* Laser printer v.s. inkjet
